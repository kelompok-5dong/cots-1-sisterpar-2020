# COTS 1 SisterPar 2020
## a . Deskripsi Aplikasi
aplikasi ini adalah program atm sederhana yang menggunakan metode RPC. <br/>
Untuk development program kami menggunakan bahasa pemrograman python, beserta modul dan library seperti xmplrc dan time.

## b . tugas anggota
Fauzi Cahya MS (Developer+Maintener)
Muhammad Farhan(Developer+Maintener)

## c . prosedur instalasi
1. Jalankan atm_server untuk memulai servernya.<br/>
2. Jalankan atm_client untuk memulai client dan menggunakan program atm.<br/>
3. pada atm_client akan ada 6 pilihan.<br/>
4. ketik 1 jika ingin cek saldo, saldo akan ditampilkan dan untuk kembali ke menu utama tekan 'Enter'<br/>
5. ketik 2 jika ingin transfer, lalu kita masukkan nomor rekening, dan jumlah uang yang ingin di transfer, tekan 'Enter' untuk kembali ke menu utama.<br/>
(Jumlah uang yang di transfer tidak boleh lebih dari saldo tabungan dan jumlah uang yang di input harus diatas 0)<br/>
6. ketik 3 jika ingin setor tunai, lalu kita masukkan uang yang ingin di setor, dan tekan 'Enter' untuk kembali ke menu utama.<br/>
(Jumlah uang yang di setor harus diatas 0)<br/>
7. ketik 4 jika ingin tarik tunai, lalu kita masukkan jumlah uang yang ingin ditarik, dan tekan 'Enter untuk kembali ke menu utama.<br/>
(Jumlah uang yang di tarik tidak boleh lebih dari saldo tabungan dan jumlah uang yang di input harus diatas 0)<br/>
8. ketik 5 jika ingin melihat mutasi, lalu tampilan mutasi akan diperlihatkan dan tekan 'Enter' untuk kembali ke menu utama.<br/>
9. ketik 6 jika ingin melihat history transaksi, masukkan bulan tertentu berdasarkan urutan bulan (contoh Januari = 1), lalu history transaksi akan ditampilkan, dan <br/>
tekan 'Enter' untuk kembali ke menu utama.<br/>