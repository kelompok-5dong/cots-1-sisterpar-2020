# import xmlrpc bagian client
import xmlrpc.client
import os
from datetime import datetime
# buat variabel untuk mengakses server. Gunakan parameter URL server yang akan diakses berupa IP dan port. Bentuk http://IP:port
s = xmlrpc.client.ServerProxy('http://localhost:8000')

def main() :

	os.system('cls')
	print("================================")
	print ("ATM KELOMPOK 5")
	print ()
	print ("1. Cek Saldo")
	print ("2. Transfer")
	print ("3. Setor Tunai")
	print ("4. Tarik Tunai")
	print ("5. Mutasi")
	print ("6. History Transaksi")
	print ()
	print("================================")
	pilih = int(input("Masukkan Pilihan : "))

	#menu cek saldo
	if (pilih == 1) :
		#memasukan nilai saldo menggunakan fungsi saldo menggunakan metode rpc pada server
		saldo = int(s.saldo())
		print()
		#menampilkan nilai saldo
		print("Saldo anda : ",saldo)
		print ()
		#kembali ke menu
		kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
		main()
    #menu transfer
	elif (pilih == 2 ) :
		rek = int(input("Masukkan Nomor Rekening : "))
		uang = int(input("Jumlah Uang : "))
		#membaca nilai saldo menggunakan fungsi saldo pada server
		saldo = s.saldo()
		if (uang <= saldo and uang > 0) :
			#memberi nilai input pada history berdasarkan tanggal dan jumlah uang yang di transfer
			transaksi = datetime.now().strftime("%d-%b-%Y(%H:%M:%S)") + " Transfer " + str(uang) + "\n"
			#memanggil fungsi updates(update saldo) dan mengurangi jumlah saldo dengan uang yang di inputkan
			s.updates(saldo-uang)
			#memanggil fungsi updateh(update history) untuk menulis pada file history berdasarkan transaksi
			s.updateh(transaksi)
			print()
			print("Transfer Berhasil!")
			print ()
			#kembali ke menu
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
		#kondisi saat saldo tidak mencukupi
		elif (uang > saldo) :
			print()
			print("Maaf saldo anda tidak cukup")
			print ()
			#kembali ke menu utama
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
		#kondisi saat inputan uang dibawah 1
		else :
			print()
			print("Jumlah uang tidak boleh dibawah 1")
			print()
			#kembali ke menu utama
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
    #menu setor tunai
	elif (pilih == 3) :
		#input jumlah uang
		uang = int(input("Jumlah Uang : "))
		#membaca saldo menggunakan metode rpc pada fungsi saldo yang ada pada server
		saldo = s.saldo()
		if (uang > 0) :
			#memberi nilai input pada history berdasarkan tanggal dan jumlah uang yang di transfer
			transaksi = datetime.now().strftime("%d-%b-%Y(%H:%M:%S)") + " Setor Tunai " + str(uang) + "\n"
			#memanggil fungsi updates(update saldo) untuk menambahkan saldo berdasarkan uang yang disetor
			s.updates(saldo+uang)
			#memanggil fungsi updateh(update history) untuk menulis pada file history berdasarkan nilai pada variabel transaksi
			s.updateh(transaksi)
			print()
			print("Setor Tunai Berhasil!")
			print ()
			#kembali ke menu utama
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
		#kondisi saat input uang dibawah 1
		else :
			print()
			print("Jumlah uang tidak boleh dibawah 1")
			print()
			#kembali ke menu utama
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
    #menu tarik tunai
	elif (pilih == 4) :
		uang = int(input("Jumlah Uang : "))
		saldo = s.saldo()
		if (uang <= saldo and uang > 0) :
			#memberi nilai input pada history berdasarkan tanggal dan jumlah uang yang di transfer
			transaksi = datetime.now().strftime("%d-%b-%Y(%H:%M:%S)") + " Tarik Tunai " + str(uang) + "\n"
			#memanggil fungsi updates(update saldo) untuk mengurangi saldo berdasarkan uang yang di input
			s.updates(saldo-uang)
			#memanggil fungsi updateh(update history) untuk menulis pada file history berdasarkan nilai pada variabel transaksi
			s.updateh(transaksi)
			print()
			print("Tarik Tunai Berhasil!")
			print()
			#kembali ke menu utama
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
		#kondisi saat saldo tidak mencukupi
		elif (uang > saldo) :
			print()
			print("Maaf saldo anda tidak cukup")
			print()
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
		#kondisi saat inputan uang dibawah 1
		else :
			print()
			print("Jumlah uang tidak boleh dibawah 1")
			print()
			#kembali ke menu utama
			kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
			main()
    #menu mutasi
	elif (pilih == 5) :
		print()
		#memanggil fungsi mutasi pada server untuk dimasukkan pada variabel data
		data = s.mutasi()
		#membaca data yang ada di mutasi sebanyak 5 history terakhir untuk ditampilkan
		for i in range(len(data)) :
			print (data[i])
		print()
		#kembali ke menu utama
		kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
		main()
    #menu history
	elif (pilih == 6) :
		print()
		bulan = int(input("Masukkan bulan (contoh Januari = 1) : "))
		#Kondisi untuk menampilkan history berdasarkan bulan dengan mencocokan string pada file history
		if bulan == 1 :
			data = s.history("-Jan-")
		elif bulan == 2 :
			data = s.history("-Feb-")
		elif bulan == 3 :
			data = s.history("-Mar-")
		elif bulan == 4 :
			data = s.history("-Apr-")
		elif bulan == 5 :
			data = s.history("-May-")
		elif bulan == 6 :
			data = s.history("-Jun-")
		elif bulan == 7 :
			data = s.history("-Jul-")
		elif bulan == 8 :
			data = s.history("-Aug-")
		elif bulan == 9 :
			data = s.history("-Sep-")
		elif bulan == 10 :
			data = s.history("-Oct-")
		elif bulan == 11 :
			data = s.history("-Nov-")
		elif bulan == 12 :
			data = s.history("-Dec-")
		else :
			main()
		#kondisi untuk cek apakah pada bulan tersebut ada transaksi atau tidak
		if (len(data) != 0) :
			print ()
			for i in range(len(data)) :
				print (data[i])
		else :
			print ()
			print ("Transaksi di bulan tersebut tidak ada!")
			print ()
		#kembali ke menu utama
		kembali = input("Tekan 'Enter' untuk kembali ke menu utama")
		main()
	else :
		main()

main()