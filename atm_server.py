# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer
# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler
import threading
# import json
import json
# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
class RequestHandler(SimpleXMLRPCRequestHandler) :
    rpc_paths = ('/RPC2',)
# Buat server
with SimpleXMLRPCServer(("localhost", 8000), requestHandler=RequestHandler) as server :
    # siapkan lock untuk mengatasi race condition
    lock = threading.Lock()
    # fungsi untuk membaca saldo
    def saldo_akun() :
        # critical section dimulai harus dilock
        lock.acquire()
        #membuka file saldo.txt menggunakan json
        with open("saldo.txt",'r') as file :
            saldo = json.load(file)
            file.close()
            lock.release()
            return (saldo)
    # register fungsi saldo_akun() sebagai saldo
    server.register_function(saldo_akun, 'saldo')
    #fungsi untuk update/write saldo
    def update_saldo(x) :
        # critical section dimulai harus dilock
        lock.acquire()
        with open("saldo.txt", 'w') as file :
            json.dump(x,file)
            file.close()
            lock.release()
            return (True)
    # register fungsi update_saldo() sebagai updates
    server.register_function(update_saldo, 'updates')
    #fungsi untuk menampilkan mutasi
    def mutasi() :
        # critical section dimulai harus dilock
        lock.acquire()
        #membaca dari history.txt
        with open("history.txt", "r") as file :
            history = file.read()
            #split data dengan jarak kebawah "ENTER"
            data = history.split('\n')
            del data[-1]
            file.close()
            lock.release()
            #Kondisi jika data pada history lebih dari 5
            if len(data) > 5 :
                #inisiasi mov dengan -5
                mov = -5
                #membuat list kosong untuk databaru
                databaru = []
                #menambahkan 5 data terakhir ke dalam list databaru
                while (mov < 0) :
                    databaru.append(data[mov])
                    mov = mov + 1
                return (databaru)
            else :
                return(data)
    # register fungsi mutasi() sebagai mutasi
    server.register_function(mutasi, 'mutasi')
    #fungsi untuk membaca history
    def history(x) :
        # critical section dimulai harus dilock
        lock.acquire()
        #membaca file history.txt
        with open("history.txt", "r") as file :
            #membuat list kosong
            his = []
            for line in file :
                # x adalah nama bulan yang akan dibaca
                if x in line :
                    #menambahkan ke list kosong
                    his.append(line)
            file.close()
            lock.release()
            return (his)
    # register fungsi history() sebagai history
    server.register_function(history, 'history')

    # membuat fungsi update history
    def update_history(x) :
        # critical section dimulai harus dilock
        lock.acquire()
        #membuka file history.txt untuk di append dan write berdasarkan variable x yang didapat dari client       
        with open("history.txt", "a") as file :
            file.write(x)
            file.close()
            lock.release()
            return (True)
    # register fungsi history() sebagai history
    server.register_function(update_history, 'updateh')

    print ("Server ATM berjalan...")
    #jalankan server
    server.serve_forever()
